function sayHello() {
  console.log("hello");
}

// bind call apply
let say_hello = () => {
  console.log("hello arrow function");
};
say_hello();

// let tinhTong = (n1, n2) => {
//   return n1 + n2;
// };
let tinhTong = (n1, n2) => n1 + n2;
// map js mdn
var n3 = tinhTong(2, 6);
console.log("😀 - n3", n3);
