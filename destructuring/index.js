// object
let dog = {
  name: "lulu mommy",
  age: 5,
  child: {
    name: "lulu baby",
    age: 1,
  },
};
let { name, age } = dog;
let { name: nameBaby, age: ageBaby } = dog.child;
console.log("😀 - nameBaby", nameBaby);
// destructuring and rename
console.log("😀 - name, age ", name, age);

// array

let colors = ["red", "blue"];
let [c1, c2] = colors;
console.log("😀 - c1, c2", c1, c2);
