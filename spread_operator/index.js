let dog1 = {
  name: "milo",
  rating: 4.5,
};

let dog2 = { ...dog1, rating: 2 };
// clone và update cùng 1 dòng
// ... => tạo địa chỉ mới
// shallow copy,deep copy
// shallow compare, deep compare

// pass by reference
// dog2.rating = 1;
console.log("😀 - dog1", dog1);
console.log("😀 - dog2", dog2);

// chunk

let colors = ["red", "blue"];
let colorsV2 = [...colors, "green", "black"];
// colorsV2.push("green");
console.log("😀 - colors", colors);
console.log("😀 - colorsV2", colorsV2);
