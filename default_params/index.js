let tinhDTB = (toan, ly, hoa = 0) => (toan + ly + hoa) / 3;

let ketQua1 = tinhDTB(5, 5);
console.log("😀 - ketQua1", ketQua1);
//

var introduce = (
  name,
  age,
  handleMessage = () => {
    console.log("Không biết nói gì hết");
  }
) => {
  console.log("Nhân viên: ", name, age);
  handleMessage();
};

var username = "Alice Nguyễn";
var userAge = 20;

var sayHello = () => {
  console.log("Hello to công ty");
};
introduce(username, userAge, sayHello);
introduce("Tommy", 22);
