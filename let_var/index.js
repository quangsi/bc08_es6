console.log("😀 - username", username);
var username = "bob";
var username = "alice";
/**
console.log("😀 - username", username);
var username = "bob";
var username = "alice";

var username
console.log("😀 - username", username);
 username = "bob";
 username = "alice";
=> es5 => cho phép tạo trùng tên biến
*/

// => es6 => ko cho phép tạo trùng tên

// console.log("😀 - account", account);
let account = "alice@123";
// let account="abc"

// cope ~ phạm vi hoạt động

// var ~ function scope
// let ~ block scope ( if )
var isLogin = true;

function checkLogin() {
  if (isLogin) {
    var message = "Success";
  } else {
    var message = "Error";
  }
  console.log("😀 - checkLogin - message", message);
}
checkLogin();

let is_login = false;
function check_login() {
  if (is_login) {
    let message = "success";
  } else {
    let message = "error";
  }
  console.log("😀 - check_login - message", message);
}

check_login();
// lỗi 1, ko lỗi 0
